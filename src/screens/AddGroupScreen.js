import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

function AddGroupScreen() {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>AddGroupScreen</Text>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  text: {
    color: '#101010',
    fontSize: 24,
    fontWeight: 'bold',
  },
});

export default AddGroupScreen;
