/* eslint-disable no-undef */
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Alert,
  SafeAreaView,
  Image,
  KeyboardAvoidingView,
} from 'react-native';
import Button from '../components/Button';
import Strings from '../const/String';
import EmailTextField from '../components/EmailTextField';
import PasswordTextField from '../components/PasswordTextField';
import Utility from '../utils/Utility';
import String from '../const/String';
import Color from '../utils/Colors';
import constants from '../const/Constants';
import DismissKeyboard from '../components/DismissKeyboard';
import Images from '../const/Images';

function SingInScreen() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [isLoading, setIsLoading] = useState('');

  validateEmailAddress = () => {
    const isValidEmail = Utility.isEmailValid(email);
    isValidEmail
      ? setEmailError('')
      : setEmailError(Strings.InvalidEmailAddress);
    return isValidEmail;
  };
  validatePasswordField = () => {
    const isValidField = Utility.isValidField(password);
    isValidField
      ? setPasswordError('')
      : setPasswordError(Strings.PasswordFieldEmpty);
    return isValidField;
  };
  return (
    <DismissKeyboard>
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <View>
          <SafeAreaView>
            <Image style={styles.logo} source={Images.logo} />

            <EmailTextField
              term={email}
              error={emailError}
              placeHolder={Strings.EmailPlaceHolder}
              onTermChange={newEmail => setEmail(newEmail)}
              onValidateEmailAddress={validateEmailAddress}
            />

            <PasswordTextField
              term={password}
              error={passwordError}
              placeHolder={Strings.PasswordPlaceHolder}
              onTermChange={newPassword => {
                setPassword(newPassword);
              }}
              onValidatePasswordField={validatePasswordField}
            />

            <Button title={Strings.Join} />
          </SafeAreaView>
        </View>
      </KeyboardAvoidingView>
    </DismissKeyboard>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Color.theme,
    alignItems: 'center',
  },
  logo: {
    alignSelf: 'center',
  },
});

export default SingInScreen;
