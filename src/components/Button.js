import React from 'react';
import {TouchableOpacity, StyleSheet, Text} from 'react-native';
import Color from '../utils/Colors';
import LinearGradient from 'react-native-linear-gradient';

const Button = props => {
  const {title = 'enter', style = {}, textStyle = {}, onPress} = props;
  return (
    <LinearGradient
      colors={['#39b54a', '#006837']}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      style={styles.button}>
      <TouchableOpacity onPress={onPress} style={[styles.button, style]}>
        <Text style={[styles.text, textStyle]}> {title}</Text>
      </TouchableOpacity>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  button: {
    display: 'flex',
    height: 50,
    borderRadius: 50,
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
    // backgroundColor: Color.uaStudiosGreen,
    shadowColor: Color.uaStudiosGreen,
    shadowOpacity: 0.4,
    shadowOffset: {height: 10, width: 10},
    shadowRadius: 20,
  },
  text: {
    fontSize: 16,
    textTransform: 'uppercase',
    color: Color.white,
  },
});

export default Button;
